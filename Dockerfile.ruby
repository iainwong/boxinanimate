##########################################################
# This Dockerfile is responsible for constructing a
# generic ubuntu based docker image with the most 
# minimal tools installed 
##########################################################

# Set base image to Ubuntu
FROM boxinanimate:generic 

# Developer info
MAINTAINER Iain Wong

################## BEGIN INSTALLATION ####################
RUN echo "Constructing boxinanimate docker image!"

RUN /bin/bash -c "source /opt/boxinanimate/lib/DEVELOPMENT.source && install_ruby"
################## END INSTALLATION ######################

# Container's User Configurations
#USER boxinanimate
