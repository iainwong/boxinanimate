<p align="center"><img width=80% src="https://media1.tenor.com/images/f380ff60b89bb9c4e50f20334dac8208/tenor.gif?itemid=3524740"></p>

# boxinanimate

Start coding your ideas NOW! The Development Environment is the premier
obstacle which prevents us from creating new software. Before anything can be
compiled or interpreted, scripted or coded - one first needs an environment,
on which to develop.

Boxinanimate is that environment. It is any environment; It can be any
environment.

The Boxinanimate Project, aims to provide fully functioning boxed environments -
without needing to change a single miniscule detail.

Boxinanimate currently provides boxed development environments for:

ENVIRONMENT | LANGUAGE | STATUS | COMMAND
------------|----------|--------|---------
Ruby        | Ruby     | :umbrella: | `docker-compose run ruby`
JDK 8/Gradle| Java     | :sunny: | `docker-compose run java`
Eclipse Neon| Java     | :sunny: | `docker-compose run eclipse`
Generic Editing | Any | :umbrella: | `docker-compose run generic`
Boxinanimate Development | N/A | :sunny: | `docker-compose run boxinanimate`

## Steps to Install
```
$ sudo ./RUNME.sh
```

## What does `RUNME.sh` do
Runme is a bootstrapping script. It does the following:
1. If not installed, install Docker
2. If not installed, install Docker-Compose
3. If not installed, install Xorg and Xserver
4. Install bash basics like: vim, git, curl
5. Symlink dotfiles
6. Generate default 4096-length SSH Key

## Steps to Contribute
```
$ git clone https://github.com/iain-wong/boxinanimate.git
$ cd boxinanimate/
$ sudo ./RUNME.sh
$ sudo docker-compose run boxinanimate tmux
```
Then make some changes and create a Pull Request!

## Etymology

This project's title comes from two words: box, and inanimate. The idea is
that - a box, could be any language specific development environment; whereas
the word - inanimate, both applies to the box environment itself, since it is
this binary, containerized image - "devoid of life", but inanimate also applies
to the process the developer usually has to undergo when first working on a
project. The developer has to "breath life" into their Development Environment
and animate it in some regard.
