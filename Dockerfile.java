##########################################################
# This Dockerfile is responsible for constructing a
# generic ubuntu based docker image with the most 
# minimal tools installed 
##########################################################

# Set base image to Ubuntu
FROM boxinanimate/generic:latest

# Developer info
MAINTAINER Iain Wong

################## BEGIN INSTALLATION ####################
USER root
RUN echo "Constructing Java boxinanimate docker image!"

RUN /bin/bash -c "source /opt/boxinanimate/lib/DEVELOPMENT.source && install_java"

RUN chown boxinanimate:boxinanimate -R /home/boxinanimate
WORKDIR /home/boxinanimate

USER boxinanimate
ENV PATH="${PATH}:/opt/gradle/gradle-4.3.1/bin"

RUN echo "export PATH=$PATH:/opt/gradle/gradle-4.3.1/bin" >> ~/.bashrc
################## END INSTALLATION ######################

# Container's User Configurations
#USER boxinanimate
