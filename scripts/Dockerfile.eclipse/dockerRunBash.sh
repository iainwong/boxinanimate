#!/bin/bash

docker run -ti --rm \
         --volumes-from homedirDemo \
	   -p 44444:44444 \
	   -p 44443:44443 \
           -e DISPLAY=$DISPLAY \
           -v /tmp/.X11-unix:/tmp/.X11-unix \
           -v "`pwd`":/workspace \
boxinanimate/eclipse:v1.0 bash
