echo "############################"
echo "#       DOCKER ECLIPSE     #"
echo "############################"
echo "Starting script..."
echo ""

echo "Starting XMing..."
start "C:\Program Files (x86)\Xming\Xming.exe" ":2 -multiwindow -clipboard -ac -dpi 120"

$ip=Get-NetIPAddress -InterfaceAlias "vEthernet (DockerNAT)" -AddressFamily IPv4|Where {$_.Ipaddress.length -gt 1}
$abc=$ip.ipaddress
echo ""
echo "Setting DISPLAY to "$abc":2.0..."
set-variable -name DISPLAY -value $abc":2.0"
echo ""
echo "Launching Eclipse..."
docker run -d --rm --name devclipse --net=host -v C:\Users\Iain\DockerData\workspace\:/home/developer/workspace -v C:\Users\Iain\DockerData\eclipse\:/home/developer/.eclipse/ -v C:\Users\Iain\DockerData\ssh\.ssh\:/home/developer/.ssh/ -v C:\Users\Iain\DockerData\.gradle\:/home/developer/.gradle/ -v C:\Users\Iain\DockerData\etc\hosts:/etc/hosts -v C:\Users\Iain\DockerData\tomcat:/home/developer/tomcat/ -e DISPLAY=$DISPLAY boxinanimate/eclipse:v1.0
