#!/bin/bash

source "./base/DOCKER.source"
source "./lib/BASH.source"

if [ "$(whoami)" != "root" ]; then
	echo "Run as root: sudo ./RUNME.sh"
	exit 1
fi

# Create directory structures
mkdir -p ~/.ssh/
mkdir -p ~/YOUR_JAVA_WORKSPACE/
mkdir -p ~/DockerData/eclipse/
mkdir -p ~/DockerData/gradle/
mkdir -p ~/DockerData/etc/
cp /etc/hosts ~/DockerData/etc/

# Copy over config files
cp ./vimrc ~/
cp ./.tmux.conf ~/
cp ./bashrc ~/

# Install docker dependencies on the host
install_docker
install_bash	
install_ssh_key
install_xserver

# Build Generic Image
#docker build -t genericimg -f Dockerfile.generic .
docker-compose build eclipse
docker-compose run eclipse
